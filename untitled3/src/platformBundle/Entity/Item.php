<?php

namespace platformBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Item
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="platformBundle\Entity\ItemRepository")
 */
class Item
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=255)
     */
    private $name;


    /**
     * @ORM\ManyToOne(targetEntity="platformBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $seller;

    /**
     * @var string
     *
     * @ORM\Column(name="Description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="PhotoUrl", type="text")
     */
    private $photoUrl;


    /**
     * @var date
     *
     * @ORM\Column(name="CreationDate", type="date")
     */
    private $creationDate;
    /**
     * @var boolean
     *
     * @ORM\Column(name="Sold", type="boolean", options={"default"=false})
     */
    private $sold;
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Item
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Item
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set photoUrl
     *
     * @param string $photoUrl
     *
     * @return Item
     */
    public function setPhotoUrl($photoUrl)
    {
        $this->photoUrl = $photoUrl;

        return $this;
    }

    /**
     * Get photoUrl
     *
     * @return string
     */
    public function getPhotoUrl()
    {
        return $this->photoUrl;
    }

    public function setSeller(User $user)
    {
        $this->seller = $user;
        return $this;
    }

    public function getSeller()
    {
        return $this->seller;
    }

    public function setSold($sold)
    {
        $this->sold = $sold;
        return $this;
    }
    public function getSold()
    {
        return $this->sold;
    }

    public function __construct()
    {
        $this->creationDate = new \DateTime();
    }
}

