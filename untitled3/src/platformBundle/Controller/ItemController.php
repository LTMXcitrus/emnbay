<?php

namespace platformBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use platformBundle\Entity\Item;

class ItemController extends Controller
{
    public function indexAction()
    {
        $repository =$this->getDoctrine()->getManager()->getRepository('platformBundle:Item');
        $items = $repository->findBy(
            array(),
            array('id' => 'desc'),
            5
        );
        return $this->render('platformBundle:Item:index.html.twig',array('items'=>$items));
    }

    public function itemAction($id)
    {
        $entityManager =$this->getDoctrine()->getManager();
        $item = $entityManager ->find("platformBundle:Item",$id);

        return $this->render('platformBundle:Item:item.html.twig',array('item'=>$item));
    }

    public function itemDetailsAction($id){
        $entityManager =$this->getDoctrine()->getManager();
        $item = $entityManager ->find("platformBundle:Item",$id);

        return $this -> render('platformBundle:Item:itemDetails.html.twig', array('item'=>$item));
    }
}
