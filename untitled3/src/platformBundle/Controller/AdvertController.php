<?php

namespace platformBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use platformBundle\Entity\User;
use platformBundle\Entity\Item;

class AdvertController extends Controller
{
    public function indexAction()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $item = $entityManager->find('platformBundle:Item', 1);
        return new Response($this -> get('templating') ->render('platformBundle:Advert:index.html.twig',array(
            'item' => $item
        )));
    }

    public function createFakeEntitiesAction()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $user = new User;
        $user->setFirstname("Matthieu");
        $user->setLastname("Lemonnier");
        $user->setPhoneNumber("0679998816");
        $user->setMail("matthieu.lemonnier45140@gmail.com");
        $user->setAddressId(1);
        $entityManager->persist($user);

        $item = new Item;
        $item->setName("Cuillière");
        $item->setDescription("La plus belle de toutes");
        $item->setSeller($user);
        $item->setSold(false);
        $item->setPhotoUrl("http://www.guydegrenne.fr/media/catalog/product/cache/1/image/650x650/9df78eab33525d08d6e5fb8d27136e95/1/4/141092_CONFIDENCE_HOTEL_CUILLERE_TABLE_01.jpg");
        $entityManager->persist($item);

        $entityManager->flush();
        return new Response("Entities created !");
    }

    public function welcomeAction()
    {
        return new Response($this -> get('templating') ->render('platformBundle:Advert:welcome.html.twig'));
    }

    public function viewAction($id,$format)
    {
        $content = $this -> get('templating') ->render('platformBundle:Advert:index.html.twig', array(
            'name' => $id,
            'format' => $format
        ));
        return new Response($content);
    }

    public function menuAction()
    {
        // On fixe en dur une liste ici, bien entendu par la suite
        // on la récupérera depuis la BDD !
        $listAdverts = array(
            array('id' => 2, 'title' => 'Recherche développeur Symfony2'),
            array('id' => 5, 'title' => 'Mission de webmaster'),
            array('id' => 9, 'title' => 'Offre de stage webdesigner')
        );

        return $this->render('platformBundle:Advert:menu.html.twig', array(
            // Tout l'intérêt est ici : le contrôleur passe
            // les variables nécessaires au template !
            'listAdverts' => $listAdverts
        ));
    }
}
